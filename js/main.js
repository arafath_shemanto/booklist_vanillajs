let title = document.querySelector(".title");
let Author = document.querySelector(".Author");
let year = document.querySelector(".year");
let add_book_btn = document.querySelector(".add_book_btn");
let booklist_table = document.querySelector(".booklist_table");

add_book_btn.addEventListener("click", function (e) {
  e.preventDefault();
  if (title.value == "" && Author.value == "" && year.value == "") {
    alert("fill every field");
  } else {
    let tr = document.createElement("tr");
    // newTitle
    let newTitle = document.createElement("th");
    newTitle.innerHTML = title.value;
    tr.appendChild(newTitle);

    // Author
    let newAuthor = document.createElement("th");
    newAuthor.innerHTML = Author.value;
    tr.appendChild(newAuthor);

    // Author
    let newyear = document.createElement("th");
    newyear.innerHTML = year.value;
    tr.appendChild(newyear);

    booklist_table.appendChild(tr);
  }
});
